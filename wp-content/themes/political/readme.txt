=== Political Theme ===
Contributors: WPComb
Requires at least:   4.5
Tested up to:        5.2
Stable tag:          1.0.5
Requires PHP:        5.6
License:             GNU General Public License v3 or later
License URI:         http://www.gnu.org/licenses/gpl-3.0.html
Tags:                flexible-header, custom-background, custom-logo, custom-menu, featured-images, sticky-post, blog, custom-header, editor-style, two-columns

=== Description ===

Political is a WordPress Theme which can be downloaded for FREE. It was crafted for Political Blogging, which includes a variety of categories, such as Political Analysts, Bloggers, People that are working on a City Hall or simply for enthusiasts.
Demo: http://wpcomb.com/themes/political
Support via Email: support@wpcomb.com


=== Installation ===

1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.


=== Frequently Asked Questions ===

= Does this theme support any plugins? =
Consulter includes support for: Contact Form 7.


==  Copyright  ==

* Political WordPress Theme, Copyright 2019 WPComb
* Political is distributed under the terms of the GNU GPL


=== Credits ===

   * Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)

   TGM Plugin Activation
   Licensed under the GPL
   http://tgmpluginactivation.com/#license

------------------------------------------------------

JS Files
   
   Bootstrap - https://github.com/twbs/bootstrap/
   Licensed under the MIT license
      
   classie.js - https://github.com/ded/bonzo#licence--copyright
   Licensed under the MIT license
      
   jQuery Sticky (jquery.sticky.js)- https://github.com/garand/sticky/blob/master/LICENSE.md
   Licensed under the MIT license

   Loaders.css.js - https://github.com/ConnorAtherton/loaders.css/#licence
   Licensed under the MIT license
   
   OWL Carousel JS - https://github.com/evenicoulddoit/owl-carousel#license
   Licensed under the MIT license
   
   jQuery validate - https://github.com/jquery-validation/jquery-validation#license
   Licensed under the MIT license
   
   
-----------------------------------------------------
CSS Files
   Animate.css - https://github.com/daneden/animate.css#license
   Licensed under the MIT license

   Bootstrap - https://github.com/twbs/bootstrap#copyright-and-license
   Licensed under the MIT license

   Font-Awesome - https://github.com/FortAwesome/Font-Awesome#license
   Licensed under the MIT&CC license

   Loaders.css - https://github.com/ConnorAtherton/loaders.css/#licence
   Licensed under the MIT license

   OWL Carousel - https://github.com/evenicoulddoit/owl-carousel#license
   Licensed under the MIT license

   simple-line-icons.css - https://github.com/thesabbir/simple-line-icons#license
   Licensed under the MIT license

   
------------------------------------------------------
Fonts
   Poppins:
   https://fonts.google.com/attribution

   Simple-Line-Icons - https://github.com/thesabbir/simple-line-icons#license
   Licensed under the MIT license
   
------------------------------------------------------
Photos

   Article Image (Used on Theme Screenshot / Slider), Copyright pxhere.com
   License: CC0 1.0 Universal (CC0 1.0)
      ✓ Free for personal and commercial use
      ✓ No attribution required
   Source: https://pxhere.com/en/photo/1209303

   Article Image (Used on Theme Screenshot / Slider), Copyright pxhere.com
   License: CC0 1.0 Universal (CC0 1.0)
      ✓ Free for personal and commercial use
      ✓ No attribution required
   Source: https://pxhere.com/en/photo/747227

   Article Image (Used on Theme Screenshot / Slider), Copyright pxhere.com
   License: CC0 1.0 Universal (CC0 1.0)
      ✓ Free for personal and commercial use
      ✓ No attribution required
   Source: https://pxhere.com/en/photo/834552

------------------------------------------------------

== Documentation ==

Live Documentation can be checked here: http://docs.wpcomb.com/political
   
------------------------------------------------------

== Changelog ==

= 1.0.4 - 29 JUN 2019 =
* Initial Release