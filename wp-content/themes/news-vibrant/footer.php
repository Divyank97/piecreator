<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package CodeVibrant
 * @subpackage News Vibrant
 * @since 1.0.0
 */

?>

		</div><!-- .cv-container -->
	</div><!-- #content -->

	<?php
		/**
	     * news_vibrant_footer hook
	     * @hooked - news_vibrant_footer_start - 5
	     * @hooked - news_vibrant_footer_widget_section - 10
	     * @hooked - news_vibrant_bottom_footer_start - 15
	     * @hooked - news_vibrant_footer_site_info_section - 20
	     * @hooked - news_vibrant_footer_menu_section - 25
	     * @hooked - news_vibrant_bottom_footer_end - 30
	     * @hooked - news_vibrant_footer_end - 35
	     *
	     * @since 1.0.0
	     */
	  //  do_action( 'news_vibrant_footer' );
	?>
<div style="display : flex ; flex-flow : row ; background : #1e6dad; padding : 20px 20px;">
<div style="display : flex ; flex-flow : row ; flex-grow : 2; justify-content: center; align-items : center;">
<h2 style="color : #ffffff ; font-size : 18px; margin : 0;">© Copyright 2004 - 2019 </h2>
<a href="/"><h2 style="color : #ffffff ; font-size : 16px; padding: 0px 10px; margin : 0;">Picreator Enterprises Ltd</h2></a>
</div>
<div style="display : flex ; flex-flow : row ; flex-grow : 2; justify-content: center; align-items : center;">
<h2 style="color : #ffffff ; font-size : 18px; margin : 0;">	<a href="/?page_id=102"><span style="color : #ffffff ; font-size : 18px;">Contact Us</span></a>  - 	<a href = "#"><span style="color : #ffffff ; font-size : 18px;">Terms and Conditions</span></a> - 	<a href="/?page_id=97 "><span style="color : #ffffff ; font-size : 18px;">FAQ</span></a></h2>
<a href="https://www.instagram.com/picreatorenterprises/?hl=en" target="_blank" style="padding : 0px 0px 0px 20px ;"><img src="wp-content/uploads/2020/02/instagram (1).png" alt="divyank" style="width : 30px ; height : auto; "></a>
<a href= "https://uk.linkedin.com/in/michelle-burns-9b9944138" target="_blank" style="padding : 0px 0px 0px 10px ;"><img src="wp-content/uploads/2020/02/linkedin (1).png" alt="divyank" style="width : 30px ; height : auto; "> </a>
</div>

</div><!-- #page -->

<?php
	/**
     * news_vibrant_after_page hook
     *
     * @since 1.0.0
     */
    do_action( 'news_vibrant_after_page' );

    wp_footer();
?>

</body>
</html>
