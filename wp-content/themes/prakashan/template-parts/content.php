<?php
/**
 * Template part for displaying posts.
 *
 * @link https://codex.wordpress.org/Template_Hierarchy
 *
 * @package prakashan
 */

?>

<article id="post-<?php the_ID(); ?>" <?php post_class(); ?>>

	<header class="entry-header">
		<?php
			
		the_title( '<h2 class="entry-title"><a href="' . esc_url( get_permalink() ) . '" rel="bookmark">', '</a></h2>' );
		
		if ( has_post_thumbnail() && ! post_password_required() ) { ?>
        			<figure class="entry-thumbnail">
        			<?php if (is_singular()) { ?> 
	            		<?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?>
        			<?php } else { ?>
	        			<a href="<?php the_permalink(); ?>">
	            		<?php the_post_thumbnail('large', array('class' => 'img-responsive')); ?>
	            		</a>
	            	<?php } ?>
				</figure>				
		<?php } //.entry-thumbnail	
			
		if ( 'post' === get_post_type() ) : ?>
		<div class="entry-meta">
			<?php prakashan_posted_on(); ?>
		</div><!-- .entry-meta -->
		<?php
		endif; ?>
	</header><!-- .entry-header -->

	<div class="entry-content">
		<?php
		
			if ( !is_singular() && has_excerpt() ):
			
				the_excerpt();
			
			else:

				the_content( sprintf(
					/* translators: %s: Name of current post. */
					wp_kses( __( 'Continue Reading %s <span class="meta-nav">&rarr;</span>', 'prakashan' ), array( 'span' => array( 'class' => array() ) ) ),
					the_title( '<span class="screen-reader-text">"', '"</span>', false )
				) );
	
				wp_link_pages( array(
					'before' => '<div class="page-links">' . esc_html__( 'Pages:', 'prakashan' ),
					'after'  => '</div>',
				) );
				
			endif;
		?>
	</div><!-- .entry-content -->
	
	<?php
	if ( is_singular() ) : ?>
	<footer class="entry-footer">
		<p><?php prakashan_entry_footer(); ?></p>
	</footer><!-- .entry-footer -->
	<?php
	endif; ?>
</article><!-- #post-## -->
