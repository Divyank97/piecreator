<?php
/**
 * The header for our theme.
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package prakashan
 */

?><!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<link rel="profile" href="http://gmpg.org/xfn/11">
<link rel="pingback" href="<?php bloginfo( 'pingback_url' ); ?>">

<?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<div id="page" class="site">
	<a class="skip-link screen-reader-text" href="#main"><?php esc_html_e( 'Skip to content', 'prakashan' ); ?></a>

	<header id="masthead" class="site-header" role="banner">
	
		<?php if (display_header_text()==true): ?>
		<div class="site-branding">
			<?php
			if ( is_front_page() && is_home() ) : ?>
				<h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
			<?php else : ?>
				<p class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></p>
			<?php
			endif;

			$prakashan_description = get_bloginfo( 'description', 'display' );
			if ( $prakashan_description || is_customize_preview() ) : ?>
				<p class="site-description"><?php echo esc_attr($prakashan_description); /* WPCS: xss ok. */ ?></p>
			<?php endif; ?>
			
		</div><!-- .site-branding -->
		<?php endif; ?>

		<?php
		if ( get_header_image() != '' ):	?>
		<div class="site-header-image">
		<img src="<?php echo( esc_url(get_header_image()) ); ?>" alt="<?php echo( esc_attr(get_bloginfo( 'title' )) ); ?>" />
		</div>
		<?php endif; ?>

		<?php
			if ( has_nav_menu( 'primary' ) ) : ?>
		<nav id="site-navigation" class="main-navigation" role="navigation">
			<button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span class="action-text"><?php esc_attr_e( 'Menu', 'prakashan' ); ?></span></button>
			<?php
			wp_nav_menu(
				array(
					'theme_location' => 'primary',
					'menu_id' => 'primary-menu',
					'fallback_cb' => FALSE,
					)
			);
			?>
		</nav><!-- #site-navigation -->

		<?php endif; ?>
		
	</header><!-- #masthead -->

	<div id="content" class="site-content">
