<?php
/**
 * The template for displaying the footer.
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package prakashan
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer" role="contentinfo">
		<div class="site-info">
			<a href="<?php echo esc_url( __( 'https://wordpress.org/', 'prakashan' ) ); ?>">
			<?php /* translators: %s is WordPress */
			printf( esc_html__( 'Proudly powered by %s', 'prakashan' ), 'WordPress' ); ?></a>
			<span class="sep"> | </span>
			<?php /* translators: %1$s is theme name, %2$s is theme author info */
			printf( esc_html__( 'Theme: %1$s by %2$s.', 'prakashan' ), 'Prakashan', '<a href="http://reduxthemes.com" rel="designer">ReduxThemes.com</a>' ); ?>
		</div><!-- .site-info -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
