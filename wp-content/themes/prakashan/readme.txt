=== Prakashan WP Theme ===
Contributors: reduxthemes
Author: ReduxThemes.com
Author URI: http://reduxthemes.com
Requires at least: WordPress 4.7
Tested up to: WordPress 5.1.1
Requires PHP: 5.6
Stable tag: 1.0.6
License: GPLv2 or later
License URI: http://www.gnu.org/licenses/gpl-2.0.html
Tags: translation-ready, custom-background, theme-options, custom-menu, post-formats, threaded-comments


A simple, elegant and distraction-free WP theme for writers/bloggers who want to highlight their content without any clutter. It has just one column, without any sidebar. The typography has been chosen to keep readers focused on your articles & blog posts.

== Credits ==

Prakashan is based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc.
Underscores is distributed under the terms of the GNU GPL v2 or later.

== Images ==
Images via Stocksnap.io, License @ CC0
- https://stocksnap.io/photo/J8HP1PXSEJ (Typewriter)

== Description ==

A simple, elegant and distraction-free WP theme for writers/bloggers who want to highlight their content without any clutter. It has just one column, without any sidebar. The typography has been chosen to keep readers focused on your articles & blog posts.

== Installation ==
	
1. In your admin panel, go to Appearance > Themes and click the Add New button.
2. Click Upload and Choose File, then select the theme's .zip file. Click Install Now.
3. Click Activate to use your new theme right away.

== Frequently Asked Questions ==

= Does this theme support any plugins? =

Prakashan includes support for Infinite Scroll in Jetpack.

== Changelog ==

= 1.0 - May 12 2015 =
* Initial release

== Credits ==

* Based on Underscores http://underscores.me/, (C) 2012-2016 Automattic, Inc., [GPLv2 or later](https://www.gnu.org/licenses/gpl-2.0.html)
* normalize.css http://necolas.github.io/normalize.css/, (C) 2012-2016 Nicolas Gallagher and Jonathan Neal, [MIT](http://opensource.org/licenses/MIT)
